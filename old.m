% Polynomial Type A
% dx/dt = p0 + y - zy
% dy/dt = p1 + z - xz
% dz/dt = p2 + x - yz

clear all;
clc;
close all;

p = [1.586 1.124 0.281];
type = 'y0z';

%poly_a_x0y = @(p0, p1, p2, x, y) [p0 + y; p1; p2 + x];
poly_a_x0y = @(p0, p1, p2, x, y) [p0; p1 - x; p2 + x - y];
%poly_a_x0z = @(p0, p1, p2, x, z) [p0; p1 + z - x*z; p2 + x];
poly_a_x0z = @(p0, p1, p2, x, z) [p0 - z; p1 + z - x*z; p2 + x - z];
%poly_a_y0z = @(p0, p1, p2, y, z) [p0 + y - z * y; p1 + z; p2 - y * z];
poly_a_y0z = @(p0, p1, p2, y, z) [p0 + y - z*y; p1; p2 - y*z];


% poly_a = @(p0, p1, p2, x, y) [p0; p1 - x; p2 + x - y];


sys = @(f, p, x) f(p(1), p(2), p(3), x(1), x(2));
sys_x0y = @(t, x) sys(poly_a_x0y, p, x);
sys_x0z = @(t, x) sys(poly_a_x0z, p, x);
sys_y0z = @(t, x) sys(poly_a_y0z, p, x);


switch(type)
    case 'x0y'
        cur_sys = sys_x0y;
        vector_field_title = 'Vector field when z = 0';
        axis_1_title = 'x';
        axis_2_title = 'y';
    case 'x0z'
        cur_sys = sys_x0z;
        vector_field_title = 'Vector field when y = 0';
        axis_1_title = 'x';
        axis_2_title = 'z';
    case 'y0z'
        cur_sys = sys_y0z;
        vector_field_title = 'Vector field when x = 0';
        axis_1_title = 'y';
        axis_2_title = 'z';
    otherwise
        'Bad input'
        return;
end

%sys_x0y = @(t, x) poly_a_x0y(p(1), p(2), p(3), x(1), x(2));

figure(1);
vectorfield(cur_sys,-20:2:20,-50:2:50);

hold on;
sep=5;
for x0=-20:sep:20
    for y0=-50:sep:50
        [ts,xs] = ode45(cur_sys,[0 100],[x0 y0 0]);
        plot(xs(:,1),xs(:,2))
        

    end;
end;

        title(vector_field_title);
        xlabel(axis_1_title);
        ylabel(axis_2_title);

axis([-20 20 -50 50]);
hold off

return;

A = [0 1 0; 0 0 1; 1 0 0];
Q = eye(3);
P=lyap(A',Q);
%syms X Y Z;
%V=[X,Y,Z]*P*[X;Y;Z];

u = -2:0.1:2;
Y=0;
[X,Z] = meshgrid(u);

V = - X.*(Y./2 - X./2 + Z./2) - Y.*(X./2 - Y./2 + Z./2) - Z.*(X./2 + Y./2 - Z./2)

[px,py] = gradient(V, .2, .2);
figure(2)
contour(u,u,V,'linewidth',2)
hold on
quiver(u,u,px,py,'linewidth',1)
hold off
title(['Function V vector field when z = ',num2str(Y)]);
xlabel('x');
ylabel('y');
% axis equal;
figure(2)
surf(X,Z,V);
xlabel('x');
ylabel('y');
zlabel('V');

function vectorfield(deqns,xval,yval,t)
    if nargin==3
        t=0;
    end
    m=length(xval);
    n=length(yval);
    x1=zeros(n,m);
    y1=zeros(n,m);
    for a=1:m
        for b=1:n
            pts = feval(deqns,t,[xval(a);yval(b)]);
            x1(b,a) = pts(1);
            y1(b,a) = pts(2);
        end;
    end;
    arrow=sqrt(x1.^2+y1.^2);
    quiver(xval,yval,x1./arrow,y1./arrow,.5,'r');
    axis tight;
end

function draw_3d(size, step)
poly_a = @(p0, p1, p2, x, y, z) [p0 + y - z*y, p1 + z - x*z, p2 + x - y*z];
figure(1); hold on; axis equal;

attractor = []; i = 1;
for x0=-size:step:size
    for y0=-size:step:size
        for z0=-size:step:size
            xyz = poly_a(p(1), p(2), p(3), x0, y0, z0);
            
            attractor(i, :) = xyz;
            i = i + 1;
            %plot3(xyz(1), xyz(2), xyz(3), 'r*-');  
        end
    end
end
plot3(attractor(:, 1), attractor(:, 2), attractor(:, 3), 'b-');  
end