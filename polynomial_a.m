%% Polynomial Type A
% dx/dt = p0 + y - zy
% dy/dt = p1 + z - xz
% dz/dt = p2 + x - yz

function func = polynomial_a(axis_)
    p0 = 1.586;
    p1 = 1.124;
    p2 = 0.281;
    
    polyA = @(x, y, z) [p0 + y - z*y; p1 + z - x*z; p2 + x - y*z];
    
    switch axis_
        case 'x'
            func = @(t, p) polyA(0, p(2), p(3));
        case 'y'
            func = @(t, p) polyA(p(1), 0, p(3));
        otherwise
            func = @(t, p) polyA(p(1), p(2), 0);
    end

end