clear all; clc; close all; set(0,'DefaultFigureColormap',feval('hot'));

global figures;
figures = 1;
system = @(axis_) polynomial_a(axis_);
%showProjection(system, 'x', [20 50], 5);
%showProjection(system, 'y', [20 50], 5);
showProjection(system, 'z', [20 50], 5);
%showMesh(system, 'x', 2, 0.1);
%showMesh(system, 'y', 2, 0.1);
%showMesh(system, 'z', 2, 0.1);

function showProjection(system, axis_, range, step)
    hold on;    

    figure(figGen());
    func = system(axis_);
    
    sx = range(1); sy = range(2);
    vectorfield(func,-sx:step:sx,-sy:step:sy, 0,axis_);
    for x0=-sx:step:sx
        for y0=-sy:step:sy
            [~,xs] = ode45(func,[0 100],[x0 y0 0]);
            plot(xs(:,1),xs(:,2))
        end;
    end;
    
    hold off
        
    switch axis_
        case 'x'
            a1 = 'y'; a2 = 'z';
        case 'y'
            a1 = 'x'; a2 = 'z';
        otherwise
            a1 = 'x'; a2 = 'y';
    end
    
    title(['Vector field when ', axis_, '=0']);
    xlabel(a1);
    ylabel(a2);
    axis([-sx sx -sy sy]);
    

end

function showMesh(system, axis_, range, step)
    %A = [0 1 0; 0 0 1; 1 0 0];
    %A=[0 0 -1; -1 -1 1; 1 -1 -1]
    %Q = eye(3);
    %P = lyap(A',Q);
    %syms X Y Z;
    %V=[X,Y,Z]*P*[X;Y;Z];
    
    u = -range:step:range;
    [a, b] = meshgrid(u);
    
    switch axis_
        case 'x'
            X = 0; Y = a; Z = b;
            a1 = 'y'; a2 = 'z';
        case 'y'
            X = a; Y = 0; Z = b;
            a1 = 'x'; a2 = 'z';
        otherwise
            X = a; Y = b; Z = 0;
            a1 = 'x'; a2 = 'y';
    end
    
    % Matlab converts element-wise operator to regular quetion when passing
    % as parameter so cannot put it in polynomial_a.m
    %V = -X.*(Y./2 - X./2 + Z./2) - Y.*(X./2 - Y./2 + Z./2) - Z.*(X./2 + Y./2 - Z./2);
    %V = -X.*(Y./8 - (X.*13)/8 + (Z.*5)/8) - Y.*(X./8 - (Y.*5)/8 + Z./8) - Z.*((X.*5)/8 + Y./8 - Z.*1);
    V = Z.^2 + 1/8*(13*X.^2+5*Y.^2-X.*Y.*2-Z.*Y.*2-X.*Z.*5-Z.*5);

    [px,py] = gradient(V, .2, .2);
    figure(figGen())
    contour(u,u,V,'linewidth',2)
    colorbar;
    hold on
    quiver(u,u,px,py,'linewidth',1)
    hold off
    title(['Function V vector field when ', axis_, '=0']);
    xlabel(a1);
    ylabel(a2);
    axis equal;
    figure(figGen())
    surf(a,b,V); 
    title(['Surface when ', axis_, '=0']);
    colorbar
    xlabel(a1);
    ylabel(a2);
    zlabel('V');
end

function fig = figGen()
    global figures;
    fig = figures;
    figures = figures + 1;
end

function vectorfield(deqns,xval,yval,t, axis_)
    if nargin==3
        t=0;
    end
    m=length(xval);
    n=length(yval);
    x1=zeros(n,m);
    y1=zeros(n,m);
    for a=1:m
        for b=1:n   
            switch axis_
                case 'x'
                    pts = feval(deqns,t,[0;xval(a);yval(b)]);
                case 'y'
                    pts = feval(deqns,t,[xval(a);0;yval(b)]);
                otherwise
                    pts = feval(deqns,t,[xval(a);yval(b);0]);
            end
            
            %pts = feval(deqns,t,[xval(a);yval(b)]);
            x1(b,a) = pts(1);
            y1(b,a) = pts(2);
        end;
    end;
    arrow=sqrt(x1.^2+y1.^2);
    quiver(xval,yval,x1./arrow,y1./arrow,.5,'r');
    axis tight;
end